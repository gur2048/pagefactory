﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace YotaSlider.Tests.Driver
{
    public static class Extensions
    {
        public static IWebElement SafeFindElement(this IWebDriver driver, By by)
        {
            ReadOnlyCollection<IWebElement> listOfWebElements = driver.FindElements(by);

            foreach (var element in listOfWebElements)
            {
                if (listOfWebElements.Count > 0)
                {
                    return element;
                }
            }
            throw new NoSuchElementException("Unable to find element with " + by);
        }

        public static bool HasElement(this IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine("Unable to find element with " + by + "StackTrace: " + e);
                return false;
            }

            return true;
        }

        public static bool HasElement(this IWebDriver driver, IWebElement webElement)
        {
            try
            {
                return webElement.Displayed;

            }
            catch (NoSuchElementException e)
            {
                Console.WriteLine("Unable to find element with " + webElement + "StackTrace: " + e);
                return false;
            }
        }


        //==========commented=========
        /*
        public static bool TypeDelay(this IWebDriver driver, By by, string type)
        {
            //int count = 0;

            IWebElement d = driver.SafeFindElement(by);
            if (d == null) throw new ArgumentNullException(nameof(d));
            while (true)
            {
                if (driver.SafeFindElement(by).Text.ToLower().Equals(type))
                {
                    return true;
                }
                else
                {
                    //count++;
                    System.Threading.Thread.Sleep(100);
                }
            }
            Console.WriteLine("Cant type in element");
            return false;
        }

           public static IWebElement FindElement(this IWebDriver remoteDriver, By by, int timeoutInSeconds = 0)
           {
               if (timeoutInSeconds > 0)
               {
                   var wait = new WebDriverWait(remoteDriver, TimeSpan.FromSeconds(timeoutInSeconds));
                   return remoteDriver.FindElement(by);
               }
               return remoteDriver.FindElement(by);
           }

           public static T WaitUntil<T>(this IWebDriver browser, Func<IWebDriver, T> condition, int timeout = 5)
           {
               var wait = new WebDriverWait(browser, new TimeSpan(0, 0, timeout));
               return wait.Until(condition);
           }

           public static IWebElement WaitForElement(this IWebElement element)
           {
               var remoteDriver = ((IWrapsDriver)element).WrappedDriver;

               new WebDriverWait(remoteDriver, TimeSpan.FromSeconds(60)).Until((d) => element.Displayed);

               return element;
           }*/

        /*public static Func<IWebDriver, IWebElement> ElementIsVisibleCustom(this ExpectedConditions condition, IWebElement element)
        {
            return (remoteDriver) =>
            {
                try
                {
                    if (element.Displayed)
                    {
                        return element;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (StaleElementReferenceException)
                {
                    return null;
                }
            };
        }
*/


    }
}