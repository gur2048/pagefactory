﻿using System;
using System.IO;
using System.Linq;

namespace YotaRegresTest.Utils
{
    public class CommandsExecutor
    {
        private System.Diagnostics.Process _process;
        private System.Diagnostics.ProcessStartInfo _startInfo;
        private string _fileName;
        private string _path;

        public CommandsExecutor()
        {
            _process = new System.Diagnostics.Process();
            _startInfo = new System.Diagnostics.ProcessStartInfo();
        }

        private string GetTestDataFolder(string testDataFolder)
        {
            string startupPath = AppDomain.CurrentDomain.BaseDirectory;
            var pathItems = startupPath.Split(Path.DirectorySeparatorChar);
            string projectPath = string.Join(Path.DirectorySeparatorChar.ToString(),
                pathItems.Take(pathItems.Length - 3));
            return Path.Combine(projectPath, testDataFolder);
        }

        public void ExecuteCmdCommand(string command)
        {
            _startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            _startInfo.FileName = "cmd.exe";
            _startInfo.Arguments = command;
            _process.StartInfo = _startInfo;
            _process.Start();
        }

        public void CloseCmdInstance()
        {
            _process.Close();
        }

        public string GetCommandRunYotaSliderInstance()
        {
            _fileName = "test-slider-1.0.0-SNAPSHOT.jar";
            _path = GetTestDataFolder("Data\\");
            return string.Format("/c java -jar {0}{1}", _path, _fileName);
        }
    }
}