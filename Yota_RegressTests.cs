﻿

using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using YotaRegresTest.Utils;
using YotaSlider.Tests.Driver;
using YotaSlider.Tests.Pages;

namespace YotaRegresTest
{
    [TestFixture]
    public class YotaRegressTests : AbstractTest
    {
        private CommandsExecutor commandsExecutor;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            commandsExecutor = new CommandsExecutor();
            commandsExecutor.ExecuteCmdCommand(commandsExecutor.GetCommandRunYotaSliderInstance());
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            commandsExecutor.CloseCmdInstance();
        }

        [SetUp]
        public void TestFixtureSetUp()
        {
            mainPage.OpenPage<MainPage>();
        }

        [TearDown]
        public void TearDown()
        {
            mainPage.DoReset<MainPage>();
        }

        [Test]
        public void OpenMainPage()
        {
            mainPage.OpenPage<MainPage>();
            Assert.IsTrue(driver.HasElement(mainPage._balance));
        }

        [Test]
        [TestCase(5000, ExpectedResult = 5000)]
        public int AddToBalance(int amount)
        {
            mainPage.OpenPage<MainPage>();
            mainPage.DoAddBalance<MainPage>(amount);
            return mainPage.GetBalance();
        }

        [Test]
        [TestCase(1400, ExpectedResult = 1400)]
        public int ActivateService(int servicePrice)
        {
            mainPage.OpenPage<MainPage>();
            int beforeServicePrice = mainPage.GetCurrentServicePrice();
            mainPage.DoAddBalance<MainPage>(servicePrice)
                .DoActivateSevice<MainPage>(servicePrice);
            int afterServicePrice = mainPage.GetCurrentServicePrice();
            return afterServicePrice - beforeServicePrice;
        }

        [Test]
        [TestCase(1400, 0, ExpectedResult = 0)]
        public int ActivationFreeService(int servicePrice, int freeServicePrice)
        {
            mainPage.OpenPage<MainPage>();
            int beforeServicePrice = mainPage.GetCurrentServicePrice();
            mainPage.DoAddBalance<MainPage>(servicePrice)
                .DoActivateSevice<MainPage>(servicePrice)
                .DoActivateSevice<MainPage>(freeServicePrice);
            int afterServicePrice = mainPage.GetCurrentServicePrice();
            return afterServicePrice - beforeServicePrice;
        }

        [Test]
        [TestCase(-5000, ExpectedResult = true)]
        public bool negativeValidationTest(int servicePrice)
        {
            mainPage.OpenPage<MainPage>();
            int beforeServicePrice = mainPage.GetBalance();
            mainPage.DoAddBalance<MainPage>((beforeServicePrice * -1) + servicePrice);
            int afterServicePrice = mainPage.GetBalance();
            return afterServicePrice >= 0;
        }

        [Test]
        [TestCase(300, 1400, ExpectedResult = true)]
        public bool banActivationWithNotEnoughBalance(int servicePrice, int sliderPos)
        {
            mainPage.OpenPage<MainPage>();

            mainPage.DoAddBalance<MainPage>(servicePrice).
                MoveSliderToNewServicePrice<MainPage>(sliderPos);    
            return mainPage.IsPurchaseButtonDisable();
        }

        [Test]
        [TestCase(300, ExpectedResult = 30)]
        public int ActivateServiceDayCounts(int servicePrice)
        {
            mainPage.OpenPage<MainPage>()
                .DoAddBalance<MainPage>(servicePrice)
                .DoActivateSevice<MainPage>(servicePrice);
            return mainPage.GetCurrentServiceDaysCount();

        }



    }
}