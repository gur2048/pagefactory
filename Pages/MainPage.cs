﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using YotaSlider.Tests.Driver;

namespace YotaSlider.Tests.Pages
{
    public class MainPage : BasePage
    {
        [FindsBy(How = How.CssSelector, Using = "div>a[data-bind*='doPayment']")] private IWebElement _doPaymentButton;

        [FindsBy(How = How.CssSelector, Using = "div>a[data-bind*='doReset']")] private IWebElement _doResetButton;

        [FindsBy(How = How.CssSelector, Using = "div>a[data-bind*='doPurchase']")] private IWebElement _doPurchaseButton;

        [FindsBy(How = How.CssSelector, Using = "span[data-bind*='balance']")] internal IWebElement _balance;

        [FindsBy(How = How.CssSelector, Using = "input[data-bind*='amount']")] private IWebElement _fieldOfPayment;

        [FindsBy(How = How.CssSelector, Using = "a[data-bind*='moveLeft']")] private IWebElement _sliderMoveLeft;

        [FindsBy(How = How.CssSelector, Using = "a[data-bind*='moveRight']")] private IWebElement _sliderMoveRight;

        [FindsBy(How = How.CssSelector, Using = "strong[data-bind*='currentCost']")] private IWebElement _newServicePrice;

        [FindsBy(How = How.CssSelector, Using = "div[class='tariff'] *.time>strong")] private IWebElement _newServiceDaysCount;

        [FindsBy(How = How.CssSelector, Using = "strong[data-bind*='currentSpeed']")] private IWebElement _newServiseSpeed;

        [FindsBy(How = How.CssSelector, Using = "strong[data-bind*='previousCost']")] private IWebElement _currentServicePrice;

        [FindsBy(How = How.CssSelector, Using = "div[class='tariff unavailable'] *.time>strong")] private IWebElement _currenServiceDaysCount;

        [FindsBy(How = How.CssSelector, Using = "strong[data-bind*='previousSpeed']")] private IWebElement _currenServiseSpeed;

        private IWebDriver driver;
        protected WebDriverWait wait;
        protected String url = "http://localhost:4567/index.html";

        public MainPage(IWebDriver driver) : base(driver)
        {
            this.driver = driver;

            RetryingElementLocator retry = new RetryingElementLocator(driver, TimeSpan.FromSeconds(5));
            PageFactory.InitElements(retry.SearchContext, this);
        }

        public T OpenPage<T>() where T : BasePage
        {
            driver.Navigate().GoToUrl(url);
            return this as T;
        }

        public T MoveSliderToNewServicePrice<T>(int ServicePrice) where T : BasePage
        {
            bool ServicePriceIsSet = false;

            while (!ServicePriceIsSet)
            {
                int actualServicePriceAsInt = Int32.Parse(_newServicePrice.Text);

                if (actualServicePriceAsInt != ServicePrice)
                {
                    if (actualServicePriceAsInt < ServicePrice)
                    {
                        this.clickElementBySendKey<T>(_sliderMoveRight);
                    }
                    else
                    {
                        this.clickElementBySendKey<T>(_sliderMoveLeft);
                    }
                }
                else
                {
                    ServicePriceIsSet = true;
                }
            }
            return this as T;
        }

        public T DoAddBalance<T>(int amount) where T : BasePage
        {
            Int32 beforeDoAddBalance = Int32.Parse(_balance.Text);
            Int32 afterDoAddBalance;
            int count = 0;
            string g = amount.ToString();
            this.typeInElement<T>(_fieldOfPayment, amount.ToString())
                .clickElementBySendKey<T>(_doPaymentButton);
            while (count < 20)
            {
                afterDoAddBalance = Int32.Parse(_balance.Text);
                if (afterDoAddBalance == beforeDoAddBalance)
                {
                    return this as T;
                }
                else
                {
                    count++;
                    System.Threading.Thread.Sleep(100);
                }
            }
            return this as T;
        }

        public T DoPushPurchaseButton<T>() where T : BasePage
        {
            this.clickElementBySendKey<T>(_doPurchaseButton);
            return this as T;
        }

        public T DoActivateSevice<T>(int servicePrice) where T : BasePage
        {
            this.MoveSliderToNewServicePrice<T>(servicePrice);
            this.DoPushPurchaseButton<T>();
            return this as T;
        }

        public int GetBalance()
        {
            int balance = 0;
            try
            {
                balance = Int32.Parse(_balance.Text);
                return balance;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cant get balance, StackTrace is: " + e);
                return 0;
            }
        }

        public T DoReset<T>() where T : BasePage
        {
            this.clickElementBySendKey<T>(_doResetButton);
            return this as T;
        }

        public int GetCurrentServicePrice()
        {
            int CurrentServicePrice = 0;
            try
            {
                CurrentServicePrice = Int32.Parse(_currentServicePrice.Text);
                return CurrentServicePrice;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cant get current service price, StackTrace is: " + e);
                return 0;
            }
        }

        public int GetCurrentServiceDaysCount()
        {
            int currenServiceDaysCount = 0;
            try
            {
                currenServiceDaysCount = Int32.Parse(_currenServiceDaysCount.Text);

                return currenServiceDaysCount;
            }
            catch (Exception e)
            {
                Console.WriteLine("Cant get current service days count, StackTrace is: " + e);
                return 0;
            }
        }

        public bool IsPurchaseButtonDisable()
        {
            int CurrentServicePrice = 0;
            return _doPurchaseButton.GetAttribute("class").ToLower().Contains("disable");
        }
    }




    //    public class MainPage : BasePage
    //    {
    //        public string _doPaymentButton = "div>a[data-bind*='doPayment']";
    //        public string _doResetButton = "div>a[data-bind*='doReset']";
    //        public string _doPurchaseButton = "div>a[data-bind*='doPurchase']";
    //        public string _balance = "span[data-bind*='balance']";
    //        public string _fieldOfPayment = "input[data-bind*='amount']";
    //        public string _sliderMoveLeft = "a[data-bind*='moveLeft']";
    //        public string _sliderMoveRight = "a[data-bind*='moveRight']";
    //        public string _newServicePrice = "strong[data-bind*='currentCost']";
    //        string _newServiceDaysCount = "div[class='tariff'] *.time>strong";
    //        string _newServiseSpeed = "strong[data-bind*='currentSpeed']";
    //        string _currentServicePrice = "strong[data-bind*='previousCost']";
    //        string _currenServiceDaysCount = "div[class='tariff unavailable'] *.time>strong";
    //        string _currenServiseSpeed = "strong[data-bind*='previousSpeed']";
    //
    //        private IWebDriver driver;
    //        protected WebDriverWait wait;
    //        protected String url = "http://localhost:4567/index.html";
    //
    //        public MainPage(IWebDriver driver) : base(driver)
    //        {
    //            this.driver = driver;
    //
    //            PageFactory.InitElements(driver, this);
    //        }
    //
    //        public T OpenPage<T>() where T : BasePage
    //        {
    //            driver.Navigate().GoToUrl(url);
    //            return this as T;
    //        }
    //
    //        public T MoveSliderToNewServicePrice<T>(int ServicePrice) where T : BasePage
    //        {
    //            bool ServicePriceIsSet = false;
    //
    //            IWebElement _newServicePrice = driver.SafeFindElement(By.CssSelector(this._newServicePrice));
    //            IWebElement _sliderMoveLeft = driver.SafeFindElement(By.CssSelector(this._sliderMoveLeft));
    //            IWebElement _sliderMoveRight = driver.SafeFindElement(By.CssSelector(this._sliderMoveRight));
    //
    //            while (!ServicePriceIsSet)
    //            {
    //                int actualServicePriceAsInt = Int32.Parse(_newServicePrice.Text);
    //
    //                if (actualServicePriceAsInt != ServicePrice)
    //                {
    //                    if (actualServicePriceAsInt < ServicePrice)
    //                    {
    //                        this.clickElementBySendKey<T>(_sliderMoveRight);
    //                    }
    //                    else
    //                    {
    //                        this.clickElementBySendKey<T>(_sliderMoveLeft);
    //                    }
    //                }
    //                else
    //                {
    //                    ServicePriceIsSet = true;
    //                }
    //            }
    //            return this as T;
    //        }
    //
    //        public T DoAddBalance<T>(int amount) where T : BasePage
    //        {
    //            IWebElement _balance = driver.SafeFindElement(By.CssSelector(this._balance));
    //            Int32 beforeDoAddBalance = Int32.Parse(_balance.Text);
    //            Int32 afterDoAddBalance;
    //            int count = 0;
    //            string g = amount.ToString();
    //            this.typeInElement<T>(By.CssSelector(_fieldOfPayment), amount.ToString())
    //                .clickElementBySendKey<T>(By.CssSelector(_doPaymentButton));
    //            while (count < 20)
    //            {
    //                afterDoAddBalance = Int32.Parse(_balance.Text);
    //                if (afterDoAddBalance == beforeDoAddBalance)
    //                {
    //                    return this as T;
    //                }
    //                else
    //                {
    //                    count++;
    //                    System.Threading.Thread.Sleep(100);
    //                }
    //            }
    //            return this as T;
    //        }
    //
    //        public T DoPushPurchaseButton<T>() where T : BasePage
    //        {
    //            this.clickElementBySendKey<T>(By.CssSelector(_doPurchaseButton));
    //            return this as T;
    //        }
    //
    //        public T DoActivateSevice<T>(int servicePrice) where T : BasePage
    //        {
    //            this.MoveSliderToNewServicePrice<T>(servicePrice);
    //            this.DoPushPurchaseButton<T>();
    //            return this as T;
    //        }
    //
    //        public int GetBalance()
    //        {
    //            int balance = 0;
    //            try
    //            {
    //                balance = Int32.Parse(driver.SafeFindElement(By.CssSelector(_balance)).Text);
    //                return balance;
    //            }
    //            catch (Exception e)
    //            {
    //                Console.WriteLine("Cant get balance, StackTrace is: " + e);
    //                return 0;
    //            }
    //        }
    //
    //        public T DoReset<T>() where T : BasePage
    //        {
    //            this.clickElementBySendKey<T>(By.CssSelector(_doResetButton));
    //            return this as T;
    //        }
    //
    //        public int GetCurrentServicePrice()
    //        {
    //            int CurrentServicePrice = 0;
    //            IWebElement element = driver.SafeFindElement(By.CssSelector(_currentServicePrice));
    //            try
    //            {
    //                CurrentServicePrice = Int32.Parse(element.Text);
    //                return CurrentServicePrice;
    //            }
    //            catch (Exception e)
    //            {
    //                Console.WriteLine("Cant get current service price, StackTrace is: " + e);
    //                return 0;
    //            }
    //        }
    //
    //        public int GetCurrentServiceDaysCount()
    //        {
    //            int currenServiceDaysCount = 0;
    //            IWebElement element = driver.SafeFindElement(By.CssSelector(_currenServiceDaysCount));
    //            try
    //            {
    //                currenServiceDaysCount = Int32.Parse(element.Text);
    //                
    //                return currenServiceDaysCount;
    //            }
    //            catch (Exception e)
    //            {
    //                Console.WriteLine("Cant get current service days count, StackTrace is: " + e);
    //                return 0;
    //            }
    //        }
    //
    //        public bool IsPurchaseButtonDisable()
    //        {
    //            int CurrentServicePrice = 0;
    //            IWebElement element = driver.SafeFindElement(By.CssSelector(_doPurchaseButton));
    //            return element.GetAttribute("class").ToLower().Contains("disable");
    //        }
    //    }
}