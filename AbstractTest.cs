﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using YotaSlider.Tests.Pages;

namespace YotaRegresTest
{
    [TestFixture]
    public class AbstractTest
    {
        protected RemoteWebDriver driver;
        protected MainPage mainPage;
        protected ChromeOptions options;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {    
            options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            driver = new ChromeDriver(options);
            mainPage = new MainPage(driver);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
                driver.Quit();
        }
    }
}
